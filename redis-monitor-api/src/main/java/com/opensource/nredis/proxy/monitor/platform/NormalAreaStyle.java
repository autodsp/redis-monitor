package com.opensource.nredis.proxy.monitor.platform;

import com.github.abel533.echarts.style.AreaStyle;

public class NormalAreaStyle extends AreaStyle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4409084628285347811L;
	
	private Object normal;

	/**
	 * @return the normal
	 */
	public Object getNormal() {
		return normal;
	}

	/**
	 * @param normal the normal to set
	 */
	public void setNormal(Object normal) {
		this.normal = normal;
	}

	
	
}
