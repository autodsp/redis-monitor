package com.opensource.nredis.proxy.monitor.dao;

import com.opensource.nredis.proxy.monitor.model.SystemApplicationComponent;

/**
* dao
*
* @author liubing
* @date 2017/01/02 15:27
* @version v1.0.0
*/
public interface ISystemApplicationComponentDao extends IMyBatisRepository<SystemApplicationComponent>,IPaginationDao<SystemApplicationComponent> {
}
