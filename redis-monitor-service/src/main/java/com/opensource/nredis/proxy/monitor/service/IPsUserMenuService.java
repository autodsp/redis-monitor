package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.PsUserMenu;

/**
* service interface
*
* @author liubing
* @date 2016/12/20 18:45
* @version v1.0.0
*/
public interface IPsUserMenuService extends IBaseService<PsUserMenu>,IPaginationService<PsUserMenu>  {
}

